
# Storyboard Results


**Table of Contents**  

- [Storyboard Results](#storyboard-results)
    - [The requirements:](#the-requirements)
    - [The usage patterns:](#the-usage-patterns)
    - [Topics for discussion:](#topics-for-discussion)
  - [Results](#results)
    - [Questions](#questions)
    - [Initial thoughts](#initial-thoughts)
    - [Wordpress](#wordpress)
  - [Published site](#published-site)
  - [Graphing](#graphing)
  - [Why not terraform?](#why-not-terraform)
  - [Is this scalable?](#is-this-scalable)
  - [References](#references)


### The requirements:
*	WordPress on GCP
*	Monitor using Prometheus
*	Graph out metrics from Prometheus with Grafana
*	Any infrastructure automation code in Git (it is not a requirement to use automation to build this, but if you do, bring the code as part of the homework so we can discuss)

### The usage patterns:
*	90% of customers live in San Francisco and Tokyo
*	Current average hourly traffic is 1k active users
*	Estimate adding 10k active users a month
*	Social marketing on the 1st & 15th of the month bring in 1,000,000 users to the website over about 24 hours
*	Cost is the least important factor
*	Uptime and low complexity of infrastructure management is most important factor

### Topics for discussion:
*	High level overview of the infrastructure you built
*	What other ways could this have been deployed?
*	How would (or did) you automate it?
*	What sites/blogs/documentation did you use while building?

## Results

### Questions
 * Is the startup in the business of standing up wordpress instances for their customers?
 * What kind of content will be served (videos, pictures, text)?
 * Will the content be dynamic or static?
 * Is this a promotional landing page?
 * Do we need to spy on the user and record their information for marketing purposes?

### Initial thoughts
 * wordpress?
 * If content is static, an object storage site with CDN would be the simplest and most scalable solution.
 * If content is not static a react or similar JS framework based site with Google functions and Google endpoints would be simpler than wordpress, k8s, app engine or whatever.
    * pair this up with [hugo](https://gohugo.io/) and CI/CD pipeline for automated deployments.
    * include spell check, link check, grammar check, and whatever else important.
    * freedom to use whatever language in the backend or cloud provider (AWS, GCP, Azure, IBM?)

### Wordpress
I choose App engine to be the host of the wordpress because the requirements seem to match what google was [offering](https://cloud.google.com/wordpress/
).  

Google App Engine
```
Deploy a scalable version of WordPress across multiple App Engine instances.
This option is similar to Kubernetes Engine, and is recommended only for
WordPress installations expecting variable traffic with high peak levels.
Choose this option if the simplicity of App Engine’s deployment is more
desirable than the flexibility of application containers.
```

Having hardly used any of google's services I wasn't sure what I was getting into.  From my perspective app engine feels like a more complicated heroku.  Heroku's deployment method is based on a normal git workflow thus making is much easier to understand and use.  App engine requires more of the user but offers a different perspective in terms of management and overall health status (more ops focused).

![](resources/app_engine_dash.png)


Since wordpress also has a central datastore I chose GCP SQL instances because they are completely managed and read replicas are just click away.  So adding a replica in a different region should help out with any regional delay. Database writes back to the primary database shouldn't be a huge deal.  After all the majority of the page views will be read only so read replicas should be all that is needed.   

One question I did have regarding App Engine is that of a static files. There is no easy CDN button so static files coming from wordpress would not be cached unless a CDN service was serving files on the edge.

I choose not to create a Kubernetes cluster because I didn't want to involve such a complex system.  Three compute instances plus a sql instance for a simple wordpress site?  Ha!  What a waste of time and money.

## Published site
The blog site was published but since I am paying for the servers I removed them after setting this up.  I used my free $300 up last year so this is costing me money.  Attached are some pictures of this amazing blog site I created.  You have never seen a blog site like this!

https://wordpress-221722.appspot.com/

![](resources/blog.png)


## Graphing
I have heard so much about prometheus but I have never had a chance to actually use it. I was a bit floored when the docs said it was easier to stand up a k8s cluster just to get prometheus running.  I stated above that I wasn't going to create a k8s cluster.  I went ahead and created the cluster because it was simpler.  This now negates my need for app engine if I have the k8s cluster.  So my stack would have looked like the following had I started with k8s:

K8s stack
1. prometheus
2. wordpress
3. helm (for deploying wordpress chart)

Graphana and prometheus was simple to get running via the [button](https://console.cloud.google.com/marketplace/kubernetes/config/google/prometheus?version=2.2&project=wordpress-221722&folder&organizationId=498778808952
)

![](resources/graphana.png)

I ran out of time trying to figure out how to export metrics from wordpress to prometheus. I am not really sure I need to export from wordpress as there seems to be some good metrics coming from app engine that could be exported to prometheus as well.  I did find some plugins for wordpress that exported the metrics so I assume I would need to use that plugin along with the prometheus push gateway.

![](resources/app_engine_dash3.png)


## Why not terraform?
I think terraform would have made this all so easy given a good template.  I looked briefly for a terraform template but they all included K8s which I didn't want at the time.  Kubernetes seems to be the dependency of many stacks these days.   Kubernetes is great software, no doubt!  But it appears  everyone is using it as a silver bullet when there are simpler solutions.  Terraform or even cloudformation should be the start any of project IMHO.

I chose not to use any terraform because I was not familiar with GCP and wanted to learn the hard way before executing a script where the internal workings are obfuscated.  Additionally, since I am only doing this procedure once there is no need to automate.   

## Is this scalable?
I don't really know if this infrastructure scales. GCP mentions auto scale but I am pessimistic when it comes to Google's statements.  A good soak test with peak load should help determine if app engine and the SQL instance could scale.  I would have to rely on some third party service to send a million unique requests to the site though.

## References
https://cloud.google.com/php/tutorials/wordpress-app-engine-flexible

https://cloud.google.com/wordpress/

https://github.com/GoogleCloudPlatform/php-docs-samples/tree/master/appengine/php72/wordpress

https://prometheus.io/blog/2018/08/23/interview-with-presslabs/

https://console.cloud.google.com/marketplace/kubernetes/config/google/prometheus?version=2.2&project=wordpress-221722&folder&organizationId=498778808952
